import json
import os
from conf.config import DATA_PATH
from task import Tasks

class TMDataService:
    @classmethod
    def _save_json(cls, data: Tasks):
        with open(DATA_PATH, "w") as f:
            json.dump(data.to_dict(), f, ensure_ascii=False, indent=4)
            
    @classmethod
    def _load_json(cls):
        try:
            f = open(DATA_PATH, "r")
        except FileNotFoundError:
            # print('IO error - file not found')
            print(f"Creating new data file at {os.path.abspath(DATA_PATH)}")
            return None
        else:
            try:
                data = json.load(f)
                return Tasks.from_dict(data)
            except json.JSONDecodeError as err:
                print("JSONDecodeError - failed to load json file", err)
            finally:
                f.close()
                
    # @classmethod
    # def _load_sql(cls, data):
    #     pass
    
    # @classmethod
    # def _save_sql(cls, data):
    #     pass
    
    @classmethod
    def save(cls, data, save_mode: str = "json"):
        if(save_mode == "json"):
            cls._save_json(data)
        # if(save_mode == "sql"):
        #     cls._save_sql(data)
        # print("saved")
        
    @classmethod
    def load(cls, load_mode: str = "json"):
        ret = None
        if load_mode == "json":
            ret = cls._load_json()
        # if load_mode == "sql":
        #     ret = cls._load_sql()
        # if ret is not None: print("loaded")
        return ret