import unittest
from utils import is_date_not_valid
from task import Task, Tasks

class TestTask(unittest.TestCase):
    def test_init(self):
        task = Task("test", "test", "01/10/2030")
        self.assertEqual(task.title, "test")
        self.assertEqual(task.description, "test")
        self.assertEqual(task.due_date, "01/10/2030")
        self.assertEqual(task.completed, False)
    
    def test_str(self):
        task = Task("test", "test", "01/10/2030")
        self.assertEqual(str(task), f"id: {task.id}\nname: test\ndescription: test\ndue_date: 01/10/2030\ncompleted: False\n---")
    
    def test_set_title(self):
        task = Task("test", "test", "01/10/2030")
        task.set_title("test2")
        self.assertEqual(task.title, "test2")
        self.assertNotEqual(task.title, "test")
    
    def test_set_description(self):
        task = Task("test", "test", "01/10/2030")
        task.set_description("test2")
        self.assertNotEqual(task.description, "test")
        self.assertEqual(task.description, "test2")
    
    def test_set_due_date(self):
        task = Task("test", "test", "01/10/2030")
        task.set_due_date("01/01/2025")
        self.assertNotEqual(task.due_date, "01/10/2030")
        self.assertEqual(task.due_date, "01/01/2025")
    
    def test_set_completed(self):
        task = Task("test", "test", "01/10/2030")
        task.set_completed(True)
        self.assertNotEqual(task.completed, False)
        self.assertEqual(task.completed, True)
    
    def test_to_dict(self):
        task = Task("test", "test", "01/01/2025")
        self.assertEqual(task.to_dict(), {"id": task.id, "title": "test", "description": "test", "due_date": "01/01/2025", "completed": False})

    def test_from_dict(self):
        task = Task("test", "test", "01/01/2025")
        dict_task = {
            "id": task.id, 
            "title": "test", 
            "description": "test", 
            "due_date": "01/01/2025", 
            "completed": False
        }
        task2 = Task.from_dict(dict_task)
        self.assertEqual(task2.title, task.title)
        self.assertEqual(task2.description, task.description)
        self.assertEqual(task2.due_date, task.due_date)
        self.assertEqual(task2.completed, task.completed)

class TestTasks(unittest.TestCase):
    def test_init(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        self.assertIsInstance(task_list, Tasks)
        
    def test_str(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        self.assertIsInstance(str(task_list), str)
        self.assertEqual(str(task_list), f"id: {task1.id}\nname: test\ndescription: test\ndue_date: test\ncompleted: False\n---\nid: {task2.id}\nname: test\ndescription: test\ndue_date: test\ncompleted: False\n---")
        
    def test_get_task_by_id(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        self.assertEqual(task_list.get_task_by_id(task1.id), task1)
        self.assertEqual(task_list.get_task_by_id(task2.id), task2)
        self.assertEqual(task_list.get_task_by_id(0), None)
    
    def test_remove_task_by_id(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        task_list.remove_task_by_id(task1.id)
        self.assertNotIn(task1, task_list)
        self.assertIn(task2, task_list)
        self.assertEqual(task_list.remove_task_by_id(0), None)
    
    def test_remove_all_tasks(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        task_list.remove_all_tasks()
        self.assertNotIn(task1, task_list)
        self.assertNotIn(task2, task_list)
    
    def test_sort_by_due_date(self):
        task1 = Task("test", "test", "01/10/2030")
        task2 = Task("test", "test", "02/10/2030")
        task_list = Tasks([task1, task2])
        task_list.sort_by_due_date()
        self.assertEqual(task_list[0], task1)
        self.assertEqual(task_list[1], task2)
        
        task_list.sort_by_due_date(reverse=True)
        self.assertEqual(task_list[0], task2)
        self.assertEqual(task_list[1], task1)
    
    def test_to_dict_gives_a_dict(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        task_list = Tasks([task1, task2])
        self.assertIsInstance(task_list.to_dict(), dict)
    
    def test_from_dict_gives_tasks(self):
        task1 = Task("test", "test", "test")
        task2 = Task("test", "test", "test")
        dict_tasks = {
            "tasks": [task1.to_dict(), task2.to_dict()]
        }
        task_list = Tasks.from_dict(dict_tasks)
        self.assertIsInstance(task_list, Tasks)
        
class testTMDataService(unittest.TestCase):
    def test_save(self):
        pass
    
    def test_load(self):
        pass    

class TestDateFormat(unittest.TestCase):
    def test_is_date_not_valid(self):
        self.assertFalse(is_date_not_valid("10/05/2025"))
        self.assertTrue(is_date_not_valid("31/12/2023"))
        self.assertTrue(is_date_not_valid("10-05-2022"))
        self.assertTrue(is_date_not_valid("30/02/2022"))
        self.assertTrue(is_date_not_valid("30/13/2022"))
        self.assertTrue(is_date_not_valid("10/05/22"))
        self.assertTrue(is_date_not_valid("10/5/2022"))
        self.assertTrue(is_date_not_valid("10/05/20222"))
        self.assertTrue(is_date_not_valid("10052022"))
    
if __name__ == "__main__":
    unittest.main()