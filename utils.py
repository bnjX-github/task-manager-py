from datetime import datetime


def is_date_not_valid(date: str):
        """
        Checks if a given date string is valid.

        Args:
            date (str): The date string to be checked.

        Returns:
            bool: True if the date is not valid, False otherwise.

        Raises:
            ValueError: If the date string is not in the format 'dd/mm/yyyy' or if the date is in the past.
        """
        try:
            date_obj: datetime = datetime.strptime(date, '%d/%m/%Y')
            valid_formatted_date: str = date_obj.strftime('%d/%m/%Y')
            if (date != valid_formatted_date):
                raise ValueError
            if (date_obj < datetime.now()):
                raise ValueError
            return False
        except ValueError as err:
            print(f"Error: {err}")
            return True