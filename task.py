import json
from conf.config import GLOBAL_PATH
from datetime import datetime

class Task():
    __count = 0
    
    with open(GLOBAL_PATH, "r") as f:
        data = json.load(f)
        __count = data["tasks_count"]
    
    def __init__(self, title: str, description: str, due_date: str, id: str = None):
        """
        Initializes a new instance of the Task class with the given title, description, and due date.

        Args:
            title (str): The title of the task.
            description (str): The description of the task. Defaults to an empty string if not provided.
            due_date (str): The due date of the task in the format "dd/mm/yyyy". Defaults to the current date if not provided.

        Returns:
            None
        """
        self.title = title
        self.description = description
        self.due_date = due_date
        self.completed = False
        self.id = id
        if (id == None):
            Task.update_count()
            self.id = "tm-"+str(Task.__count)
        
            

    def __str__(self):
        """
        Returns a string representation of the Task object, including the due date, title, and description.
        
        Returns:
            str: A string in the format "{due_date} - {title}\n{description}".
        """
        return f"id: {self.id}\nname: {self.title}\ndescription: {self.description}\ndue_date: {self.due_date}\ncompleted: {self.completed}\n---"
    
    @classmethod
    def update_count(cls):
        cls.__count += 1
        with open(GLOBAL_PATH, "w") as f:
            data = {"tasks_count": cls.__count}
            json.dump(data, f, ensure_ascii=False, indent=4)
    
    def set_title(self, title: str):
        """
        Sets the title of the task to the given value.

        Args:
            title (str): The new title of the task.

        Returns:
            None
        """
        self.title = title

    def set_description(self, description: str):
        """
        Sets the description of the task to the given value.

        Args:
            description (str): The new description of the task.

        Returns:
            None
        """
        self.description = description
        
    def set_due_date(self, due_date: str):
        """
        Sets the due date of the task to the given value.

        Args:
            due_date (str): The new due date of the task in the format "dd/mm/yyyy".

        Returns:
            None
        """
        self.due_date = due_date
    
    def set_completed(self, completed: bool):
        """
        Sets the completion status of the task to the given value.

        Args:
            completed (bool): The new completion status of the task.

        Returns:
            None
        """
        self.completed = completed
    
    def to_dict(self):
        """
        A function that converts the Task object into a dictionary representation with keys for 'title', 'description', 'due_date', and 'completed'.
        """
        return {
            "id": self.id,
            "title": self.title,
            "description": self.description,
            "due_date": self.due_date,
            "completed": self.completed
        }
        
    @classmethod    
    def from_dict(cls, data: dict):
        """
        Creates a new instance of the Task class using the values from a dictionary.

        Parameters:
            cls (Type[Task]): The class itself.
            data (dict): A dictionary containing the values for the title, description, and due_date.

        Returns:
            Task: A new instance of the Task class with the specified values.
        """
        new_task = cls(data["title"], data["description"], data["due_date"], data["id"])
        new_task.set_completed(data["completed"])
        return new_task
    
class Tasks(list[Task]):
    def __init__(self, *args, **kwargs):
        """
        Initializes a new instance of the class. This method is automatically called when a new object is created.

        Parameters:
            *args (Any): Variable length argument list.
            **kwargs (Any): Arbitrary keyword arguments.

        Returns:
            None
        """
        super().__init__(*args, **kwargs)

    def __str__(self):
        """
        Returns a string representation of the Tasks object by joining the string representations of all the tasks in the list.
        
        Returns:
            str: A string representation of the Tasks object.
        """
        return "\n".join([str(task) for task in self])
    
    def get_task_by_id(self, task_id: str):
        """
        Returns the task with the given id.

        Parameters:
            task_id (str): The id of the task to retrieve.

        Returns:
            Task: The task with the given id, or None if not found.
        """
        for task in self:
            if task.id == task_id:
                return task
        return None
    
    def remove_task_by_id(self, task_id: str):
        """
        Removes the task with the given id from the list of tasks.

        Parameters:
            task_id (str): The id of the task to remove.

        Returns:
            None
        """
        task = self.get_task_by_id(task_id)
        if task:
            self.remove(task)
        else:
            print("Task with id {task_id} not found")
        
    def remove_all_tasks(self):
        """
        Removes all the tasks from the list of tasks.

        Parameters:
            None

        Returns:
            None
        """
        self.clear()
    
    def sort_by_due_date(self, reverse: bool = False):
        """
        Sorts the tasks by due date in ascending or descending order.

        Parameters:
            reverse (bool, optional): Whether to sort in descending order. Defaults to False.

        Returns:
            None
        """
        self.sort(key=lambda task: datetime.strptime(task.due_date, "%d/%m/%Y"), reverse=reverse)

    def to_dict(self):
        """
        Converts the Tasks object into a dictionary representation.

        Returns:
            dict: A dictionary containing a list of dictionaries, where each dictionary represents a Task object.
                  The keys of the dictionaries are "tasks" and the values are a list of dictionaries, where each 
                  dictionary is the result of calling the `to_dict()` method on a Task object.
        """
        return {
            "tasks":[task.to_dict() for task in self]
        }

    @classmethod
    def from_dict(cls, data: dict):
        """
        Creates a new instance of the Tasks class from a dictionary representation.

        Parameters:
            cls (Type[Tasks]): The class itself.
            data (dict): A dictionary containing a list of dictionaries, where each dictionary represents a Task object.
                          The keys of the dictionaries are "tasks" and the values are a list of dictionaries, where each 
                          dictionary is the result of calling the `to_dict()` method on a Task object.

        Returns:
            Tasks: A new instance of the Tasks class with the specified values.
        """
        return cls([Task.from_dict(task) for task in data["tasks"]])
    
