from conf.config import build_command_line_interface
from utils import is_date_not_valid
from task import Task, Tasks
from services.tmdataservice import TMDataService


class Application:
    
    def __init__(self):
        """
        Initializes a new instance of the Application class.

        This method loads tasks from the TMDataService and assigns them to the 'tasks' attribute.
        If no tasks are loaded, an empty Tasks object is assigned instead.

        Parameters:
            None

        Returns:
            None
        """
        load_tasks = TMDataService.load()
        self.tasks = load_tasks if (load_tasks) else Tasks()    
    

    def _add_new_task(self, args):
        """
        Adds a new task to the task list based on the provided arguments.

        Parameters:
            args (Namespace): The parsed command line arguments.

        Returns:
            None

        Raises:
            ValueError: If the date provided is invalid or in the past.

        Description:
            This function adds a new task to the task list based on the provided arguments. If the 'TITLE' argument is not
            provided, the user will be prompted to enter the title of the task. Similarly, if the 'DESC' argument is not provided,
            the user will be prompted to enter the description of the task. If the 'DATE' argument is not provided, the user will
            be prompted to enter the date of the task.

            The function first checks if the provided date is valid and not in the past using the 'is_date_not_valid' method. If
            the date is not valid, the function returns without adding the task.

            If the date is valid, the function creates a new 'Task' object with the provided title, description, and date, and
            appends it to the 'tasks' list. It then prints a message indicating that a new task has been added.

            Finally, the 'save' method of the 'TMDataService' class is called to save the updated task list to the data source.
        """
        title = args.TITLE if (args.TITLE is not None) else input("Please enter the title of the task: ")
        description = args.DESC if (args.DESC is not None) else input("Please enter the description of the task: ")
        date = args.DATE if (args.DATE is not None) else input("Please enter the description of the task: ")
        
        if (is_date_not_valid(date)):
            return
        
        self.tasks.append(Task(title, description, date))
        print(f"-> new task added !")
        TMDataService.save(self.tasks)
      
    def _display_task(self, args):
        """
        Displays a task based on the provided arguments.

        Parameters:
            args (Namespace): The parsed command line arguments.

        Returns:
            None

        Description:
            This function displays a task based on the provided arguments. If the 'ID' argument is provided, it retrieves the
            task with the corresponding ID from the 'tasks' list. If the task is found, it prints the task. If the task is not
            found, it prints a message indicating that the task with the given ID was not found.

            If the 'all' argument is provided, it prints all the tasks in the 'tasks' list.

            The function does not return anything.
        """
        if (args.ID is not None):
            task_to_display = self.tasks.get_task_by_id(args.ID)
            if (task_to_display is None):
                print("Task with id {args.ID} not found")
                return
            print(task_to_display)
        elif (args.all):
            print(self.tasks)
            
    def _edit_task(self, args):
        """
        Displays a task based on the provided arguments.

        Parameters:
            args (Namespace): The parsed command line arguments.

        Returns:
            None

        Description:
            This function displays a task based on the provided arguments. If the 'ID' argument is provided, it retrieves the
            task with the corresponding ID from the 'tasks' list. If the task is found, it prints the task. If the task is not
            found, it prints a message indicating that the task with the given ID was not found.

            If the 'all' argument is provided, it prints all the tasks in the 'tasks' list.

            The function does not return anything.
        """
        if (args.ID is None):
            print("Please enter the id of the task to edit. You can use the display command to see the list of tasks")
            return
        
        task_to_edit = self.tasks.get_task_by_id(args.ID)
        if (task_to_edit is None):
            print("Task with id {args.ID} not found. You can use the display command to see the list of tasks")
            return
        
        if (args.title is None and args.description is None and args.due_date is None and args.completed is None):
            print("To modify a part of the task, please enter the corresponding flags. You can use -h or --help for more information")
            return
        
        if (args.title is not None):
            new_title = args.title if (args.title) else input(f"Actual title: {task_to_edit.title}\nPlease enter the new title of the task: ")
            task_to_edit.set_title(new_title)
            print(f"title changed to {new_title}")
        if (args.description is not None):
            new_desc = args.description if (args.description) else input(f"Actual description: {task_to_edit.description}\nPlease enter the new description of the task: ")
            task_to_edit.set_description(new_desc)
            print(f"description changed to {new_desc}")
        if (args.due_date is not None):
            new_due_date = args.due_date if (args.due_date) else input(f"Actual due date: {task_to_edit.due_date}\nPlease enter the new due date of the task. Format required dd/mm/yyyy: ")
            if (is_date_not_valid(new_due_date)):
                return
            task_to_edit.set_due_date(new_due_date)
            print(f"due date changed to {new_due_date}")
        if (args.completed is not None):
            new_completed = args.completed if (args.completed) else input(f"Actual status: {'completed' if task_to_edit.completed else 'not completed'}\nDo you want to mark the task as completed ? (yes/no): ")
            if (new_completed.lower() in ["yes", "no"]):
                task_to_edit.set_completed(new_completed.lower() == "yes")
                print(f"task is now completed" if task_to_edit.completed else f"task is not completed")
        TMDataService.save(self.tasks)
    
    def _remove_task(self, args):
        """
        Removes a task from the task list based on the provided arguments.

        Parameters:
            args (Namespace): The parsed command line arguments.

        Returns:
            None

        Description:
            This function removes a task from the task list based on the provided arguments. If the 'ID' argument is provided,
            it removes the task with the corresponding ID from the 'tasks' list. If the 'all' argument is provided, it removes
            all the tasks in the 'tasks' list.

            After removing the task(s), the 'save' method of the 'TMDataService' class is called to save the updated task list
            to the data source. Finally, a message indicating the removal of the task(s) is printed.
        """
        if (args.ID is not None):
            self.tasks.remove_task_by_id(args.ID)
            TMDataService.save(self.tasks)
            print(f"-> task {args.ID} removed !")
        elif (args.all):
            self.tasks.remove_all_tasks()
            TMDataService.save(self.tasks)
            print(f"-> all tasks removed !")
    
    def _sort_tasks(self, args):
        """
        Sorts the tasks in the task list based on their due date in ascending or descending order.

        Parameters:
            args (Namespace): The parsed command line arguments.
                - desc (bool): If True, sorts the tasks in descending order. If False, sorts them in ascending order.

        Returns:
            None

        Description:
            This function sorts the tasks in the task list based on their due date. It uses the 'sort_by_due_date' method
            of the 'tasks' object to sort the tasks. The sorting order is determined by the 'desc' attribute of the 'args'
            parameter. If 'desc' is True, the tasks are sorted in descending order. If 'desc' is False or not provided,
            the tasks are sorted in ascending order.

            After sorting the tasks, the 'save' method of the 'TMDataService' class is called to save the updated task list
            to the data source. Finally, a message indicating that the tasks have been sorted is printed.
        """
        self.tasks.sort_by_due_date(args.desc)
        TMDataService.save(self.tasks)
        print(f"-> tasks sorted !")
        
    def run(self):
        """
        Runs the application based on the command line arguments.

        This function parses the command line arguments using the `build_command_line_interface` function and executes the corresponding action based on the `command` attribute of the parsed arguments.

        Parameters:
            self (Application): The current instance of the `Application` class.

        Returns:
            None

        Description:
            - If the `command` attribute is "add", the `_add_new_task` method is called with the parsed arguments.
            - If the `command` attribute is "display", the `_display_task` method is called with the parsed arguments.
            - If the `command` attribute is "edit", the `_edit_task` method is called with the parsed arguments.
            - If the `command` attribute is "remove", the `_remove_task` method is called with the parsed arguments.
            - If the `command` attribute is "sort", the `_sort_tasks` method is called with the parsed arguments.
            - If the `command` attribute does not match any of the above, a message is printed indicating that the arguments are not valid and providing information on how to get more information.
        """
        args = build_command_line_interface()
        # print(args)
        
        if args.command == "add":
            self._add_new_task(args)
        elif args.command == "display":
            self._display_task(args)
        elif args.command == "edit":
            self._edit_task(args)
        elif args.command == "remove":
            self._remove_task(args)
        elif args.command == "sort":
            self._sort_tasks(args)
        else:
            print("Please enter the arguments. You can use -h or --help for more information")