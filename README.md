<a name="readme-top"></a>

<br />
<div align="center">
  <h1 align="center">Task Manager CLI</h1>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This is a simple task manager in command line where you can create, display, modify and remove tasks.


## Getting Started

### Prerequisites

You need to have Python and Pip already installed in your device. If you do not have Python, please install the latest version from python.org. You can check by simply running 

```sh
python --version
```

Additionally, make sure you have pip available. Check this by running.

```sh
pip --version
```

### Installation

1. Install pipenv from pypi.org using
   ```sh
   pip install pipenv --user
   ```
2. Create a repository for the project and clone the repo
   ```sh
   cd myproject
   ```
    ```sh
   git clone https://gitlab.com/bnjX-github/task-manager-py
   ```
3. Install dependencies via the Pipfile
   ```sh
   pipenv install 
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

Open a terminal and go to the root of the project. Use the following command to activate the virtual environnement provided by Pipenv:
```sh
pipenv shell
```

Then you can check how to use the cli with -h or --help:
```sh
python main.py -h
```

It will give the following result:
```sh
usage: main.py [-h] {add,display,edit,remove,sort} ...

Task Manager Help

positional arguments:
  {add,display,edit,remove,sort}
    add                 add a new task. You can use the -h or --help after the add command for more information
    display             display one task or all tasks. You can use the -h or --help after the display command for more information
    edit                edit a task. You can use the -h or --help after the edit command for more information
    remove              remove a task. You can use the -h or --help after the remove command for more information
    sort                sort tasks by due date in ascending or descending order. You can use the -h or --help after the sort command for more information

options:
  -h, --help            show this help message and exit
```

To create a task, you can use the following command:
```sh
python main.py add mytitle mydescription myduedate
```
where mytitle, mydescription and myduedate are replaced by the title, description and due date of your choice. The due date is required to be in the following format 'dd/mm/YYYY'.
It is highly recommended to use quotes when writing the title or the description as you can add multiple words in it so the terminal interprets it to be a unique argument. 
Also, if arguments are missing when you run the command, you will be promted to add the missing arguments.


To display a task, you can use the following command:
```sh
python main.py display taskid
```
where taskid is the id of the task you want to display. 
In order to display all tasks, replace the id by the flag --all  


To modify a task, you can use the following command:
```sh
python main.py edit taskid --title mytitle --description mydescription --due_date myduedate --completed taskcompleted 
```
where taskid is the id of the task you want to edit. To edit a task, there is no need to add every flag. Only one is required. For example, if you want to change the state of your task and mark this task as completed, you can use the following:
```sh
python main.py edit taskid --completed true
```
Note: Only these two lowercase values 'yes' and 'no' are valid when using the completed flag.
Similar to the add command, the due date is required to be in the 'dd/mm/YYYY' format and it is highly recommended to use quotes for the title and the description. 
If you add a flag and the argument is missing then you will be prompted to provide the missing argument


To remove a task, you can use the following command:
```sh
python main.py remove taskid
```
where taskid is the id of the task you want to remove.
Similar to the display command, add the --all flag instead of the id to remove all tasks


Finally, you can sort tasks by due date, when using the following command:
```sh
python main.py sort
```
By default the tasks will be sorted by ascending order. Add the --desc flag if you want it to be sorted by descending order.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Benjamin Gédéon - benjamingedeon1996@gmail.com

Project Link: [https://gitlab.com/bnjX-github/task-manager-py](https://gitlab.com/bnjX-github/task-manager-py)

<p align="right">(<a href="#readme-top">back to top</a>)</p>