import argparse
    
BASE_URL = ""

DATA_PATH = BASE_URL + "./data/data.json"

GLOBAL_PATH = BASE_URL + "./data/global.json"

def build_command_line_interface():
    """
    Builds the command line interface for the Task Manager application.

    This function creates an ArgumentParser object and adds subparsers for different commands.
    The subparsers are used to define different actions that can be performed by the application.
    The available commands are:
    - add: Adds a new task.
    - display: Displays one task or all tasks.
    - edit: Edits an existing task.
    - remove: Removes a task.
    - sort: Sorts tasks by due date.

    Each subparser has its own set of arguments and flags.
    The arguments are used to provide input to the corresponding command.
    The flags are used to modify the behavior of the command.

    Returns:
    argparse.Namespace: An object containing the parsed command line arguments.

    Example usage:
    args = build_command_line_interface()
    """
    parser = argparse.ArgumentParser(description="Task Manager Help")        
    subparsers = parser.add_subparsers(dest="command")
    
    parser_add = subparsers.add_parser("add", help="add a new task. You can use the -h or --help after the add command for more information")
    parser_add.add_argument("TITLE", nargs="?", help="the title of the task")
    parser_add.add_argument("DESC", nargs="?", help="the description of the task")
    parser_add.add_argument("DATE", nargs="?", help="the due date of the task")
    
    parser_display = subparsers.add_parser("display", help="display one task or all tasks. You can use the -h or --help after the display command for more information")
    parser_display.add_argument("ID", nargs="?", help="the id of the task to display. You can use the --all flag to see the list of tasks")
    parser_display.add_argument("--all", action="store_true", help="display all tasks")
    
    parser_edit = subparsers.add_parser("edit", help="edit a task. You can use the -h or --help after the edit command for more information")
    parser_edit.add_argument("ID", help="the id of the task to edit. You can use the display command to see the list of tasks")
    parser_edit.add_argument("--title", nargs="?", const=False, help="edit the title of the task")
    parser_edit.add_argument("--description", nargs="?", const=False, help="edit the description of the task")
    parser_edit.add_argument("--due_date", nargs="?", const=False, help="edit the due date of the task. Format required: dd/mm/yyyy")
    parser_edit.add_argument("--completed", nargs="?", const=False, choices=["yes", "no"], help="edit the completed status of the task")
    
    parser_remove = subparsers.add_parser("remove", help="remove a task. You can use the -h or --help after the remove command for more information")
    parser_remove.add_argument("ID", nargs="?", help="the id of the task to remove. You can use the display command to see the list of tasks")
    parser_remove.add_argument("--all", action="store_true", help="remove all tasks")
    
    parser_sort = subparsers.add_parser("sort", help="sort tasks by due date in ascending or descending order. You can use the -h or --help after the sort command for more information")
    parser_sort.add_argument("--desc", action="store_true", help="sort tasks by due date in descending order")
    
    args = parser.parse_args()
    return args

